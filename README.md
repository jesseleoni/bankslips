Project Title: BankSlips

Getting Started

O objetivo do desafio é construir uma API REST para geração de boletos que será consumido por
um módulo de um sistema de gestão financeira de microempresas.
No final do desafio vamos ter os seguintes endpoints para:
● Criar boleto
● Listar boletos
● Ver detalhes
● Pagar um boleto
● Cancelar um boleto

Prerequisites

1. Instalar o MYSQL e criar a tabela bankslips com o script:

CREATE TABLE `bankslips` (
 `id` binary(16) NOT NULL,
 `due_date` date DEFAULT NULL,
 `total_in_cents` decimal(12,2) DEFAULT NULL,
 `customer` varchar(100) DEFAULT NULL,
 `status` varchar(20) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_


2. Instalar o MAVEN e o WILDFLY 10

Running the tests: POSTMAN

A) Criar Boleto
@PostMapping("/bankslips")

B) Listar todos os Boletos	
@GetMapping("/bankslips")

C) Listar um Boleto
@GetMapping("/bankslips/{id}")

D) Alterar o Status de um boleto para: PENDING, PAID, CANCELED	
@PutMapping("/bankslips/{id}")


Versioning
1.0

Authors
Jesse Leandro Leoni

Acknowledgments

Sobre o Projeto:

Desafio Desenvolvedor Backend - ContaAzul

● Criar boleto
● Listar boletos
● Ver detalhes
● Pagar um boleto
● Cancelar um boleto

Criar boleto
Endpoint:​ POST http://localhost:8080/rest/bankslips
Esse método deve receber um novo boleto e inseri-lo em um banco de dados para ser consumido
pela própria API. Todos os campos são obrigatórios.
Ex:
{
"due_date":"2018-01-01",
"total_in_cents":"100000",
"customer":"Trillian Company",
"status":"PENDING"
}

Campo Tipo Formato
due_date date yyyy-MM-dd
total_in_cents BigDecimal valor em centavos
customer string
status string PENDING, PAID, CANCELED

Mensagens de resposta
● 201 : Bankslip created
● 400 : Bankslip not provided in the request body
● 422 : Invalid bankslip provided.The possible reasons are:
○ A field of the provided bankslip was null or with invalid values

Lista de boletos
Endpoint: ​GET http://localhost:8080/rest/bankslips/
Esse método da API deve retornar uma lista de boletos em formato JSON.
Ex:
[
{
"id":"84e8adbf-1a14-403b-ad73-d78ae19b59bf",
"due_date":"2018-01-01",
"total_in_cents":"100000",
"customer":"Ford Prefect Company",
},
{
"id":"c2dbd236-3fa5-4ccc-9c12-bd0ae1d6dd89",
"due_date":"2018-02-01",
"total_in_cents":"200000",
"customer":"Zaphod Company",
}
]
Mensagens de resposta
● 200 : Ok

Ver detalhes de um boleto

Endpoint: ​GET http://localhost:8080/rest/bankslips/{id}
Esse método da API deve retornar um boleto filtrado pelo id, caso o boleto estiver atrasado deve
ser calculado o valor da multa.
Regra para o cálculo da multa aplicada por dia para os boletos atrasados:
● Até 10 dias: Multa de 0,5% (Juros Simples)
● Acima de 10 dias: Multa de 1% (Juros Simples)
Ex:
{
"id":"c2dbd236-3fa5-4ccc-9c12-bd0ae1d6dd89",
"due_date":"2018-01-01",
"total_in_cents":"100000",
"customer":"Ford Prefect Company",
"fine":"1000",
"status":"PENDING"
}
Mensagens de resposta
● 200 : Ok
● 400 : Invalid id provided - it must be a valid UUID
● 404 : Bankslip not found with the specified id
Pagar um boleto
Endpoint​: PUT http://localhost:8080/rest/bankslips/{id}

Esse método da API deve alterar o status do boleto para PAID.
Ex:
{
"status":"PAID"
}


Campo Tipo Formato
status string PENDING, PAID, CANCELED

Mensagens de resposta
● 200 : Bankslip paid
● 404 : Bankslip not found with the specified id

Cancelar um boleto

Endpoint:​ PUT http://localhost:8080/rest/bankslips/{id}
Esse método da API deve alterar o status do boleto para CANCELED.
Ex:
{
"status":"CANCELED"
}
{} ​bankslips
Campo Tipo Formato
status string PENDING, PAID, CANCELED

Mensagens de resposta
● 200 : Bankslip canceled
● 404 : Bankslip not found with the specified id
