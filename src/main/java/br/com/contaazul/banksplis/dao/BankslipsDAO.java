package br.com.contaazul.banksplis.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.contaazul.banksplis.model.Bankslips;
import br.com.contaazul.banksplis.repository.BankslipsRepository;

@Service
public class BankslipsDAO {

	@Autowired
	BankslipsRepository bankslipsRepository;
	
	/* Save  */	
	public Bankslips save(Bankslips boleto){
		return bankslipsRepository.save(boleto);
	}
	
	/* Search All*/
	public List<Bankslips> findAll() {
		return bankslipsRepository.findAll(); 		
	}
	
	/* Get by ID*/
	public Bankslips findOne(String id) {
		return bankslipsRepository.findOne(id);
	}
	
	/* Delete by ID*/
	public void delete(Bankslips boleto){
		bankslipsRepository.delete(boleto);
	}	
				
}
