package br.com.contaazul.banksplis.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.contaazul.banksplis.dao.BankslipsDAO;
import br.com.contaazul.banksplis.model.Bankslips;

@RestController
@RequestMapping("/rest")
public class BankslipsController {
	
	@Autowired
	BankslipsDAO bankslipsDAO;
	
	/* Criar Boleto */
	@PostMapping("/bankslips")
	public Bankslips createBankslips(@Valid @RequestBody Bankslips boleto) {
		return bankslipsDAO.save(boleto);
	}
	
	/* Listar todos os Boletos */	
	@GetMapping("/bankslips")
	public List<Bankslips> getAllBankslips() {
		return bankslipsDAO.findAll();
	}

	/* Listar um Boleto */
	@SuppressWarnings("unchecked")
	@GetMapping("/bankslips/{id}")
	public ResponseEntity<Bankslips> getBankslipsById(@PathVariable(value="id") String id) {
		
		Bankslips boleto=bankslipsDAO.findOne(id);
		
		if(boleto==null){
			return (ResponseEntity<Bankslips>) ResponseEntity.notFound();		
		}
		return ResponseEntity.ok().body(boleto);
	}
	
	/* Alterar o Status de um boleto para: PENDING, PAID, CANCELED */	
	@PutMapping("/bankslips/{id}")
	public ResponseEntity<Bankslips> setStatusBankslipsById(@PathVariable(value="id") String id, String status) {
		
		Bankslips boleto=bankslipsDAO.findOne(id);
		
		if(boleto==null){
			return (ResponseEntity<Bankslips>) ResponseEntity.notFound();		
		}
		
		boleto.setStatus(status);		
		Bankslips updateBoleto=bankslipsDAO.save(boleto);
						
		return ResponseEntity.ok().body(updateBoleto);
	}
	
}
