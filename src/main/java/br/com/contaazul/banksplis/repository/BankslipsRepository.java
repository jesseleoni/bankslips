package br.com.contaazul.banksplis.repository;

import br.com.contaazul.banksplis.model.Bankslips;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankslipsRepository extends JpaRepository<Bankslips, String> {

}
